package com.example.trackscan2;

import static org.junit.Assert.assertFalse;

import org.junit.Test;

//Tests for my password input.
public class PasswordChecker {
    private String checker = "123456";

    @Test
    public void testNullPasswordReturnsFalse(){
        assertFalse(checker.isEmpty());
    }

    @Test
    public void testLongPasswordReturnsFalse(){ assertFalse(checker.length()>15); }

    @Test
    public void testShortPasswordReturnsFalse(){ assertFalse(checker.length()>6); }
}
