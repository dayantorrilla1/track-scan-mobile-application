package com.example.trackscan2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LikesActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    private DatabaseReference reference, usersReference;
    private FirebaseUser user;
    private String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likes);

        recyclerView = findViewById(R.id.likeList);

        //Database references.
        user = FirebaseAuth.getInstance().getCurrentUser();
        usersReference = FirebaseDatabase.getInstance().getReference().child("Users");



    }

    public void onStart() {
        super.onStart();

        //User is logged in.
        if (user != null) {
            userID = user.getUid();
            reference = usersReference.child(userID).child("Likes");
            displayLikedItems();
        }
    }

    //Displays liked items.
    private void displayLikedItems() {

//        //Firebase RecyclerView with adapter (displays all likes for user in the recycler view).
//        FirebaseRecyclerOptions<Track> options = new FirebaseRecyclerOptions.Builder<SubItem>()
//                .setQuery(reference, SubItem.class)
//                .build();
//
//        FirebaseRecyclerAdapter<SubItem, BasketActivity.ViewHolder> adapter
//                = new FirebaseRecyclerAdapter<SubItem, BasketActivity.ViewHolder>(options) {
//            @Override
//            protected void onBindViewHolder(@NonNull BasketActivity.ViewHolder viewHolder, int i, @NonNull SubItem review) {
//                String subItemID = getRef(i).getKey();
   }
}
