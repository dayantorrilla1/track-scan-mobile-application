package com.example.trackscan2;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.zip.Inflater;

public class ProfileFragment extends Fragment {

    private Button logout;
    private TextView emailAddress, fullName, genre;

    private String userID;
    private FirebaseUser user;
    private DatabaseReference reference;

    @Override
    //onCreateView() instead of onCreate() for fragments.
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate layout for this fragment in rootView
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        //Database variables/objects.
        user = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users");

        //Finding the views by ID and setting onclick listeners.
        logout = view.findViewById(R.id.logOut);
        fullName = view.findViewById(R.id.fullNameTitle);
        emailAddress = view.findViewById(R.id.emailAddressTitle);
        genre = view.findViewById(R.id.genreTitle);

        if (user != null) {
            userID = user.getUid();
            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getContext(), "Logging out...", Toast.LENGTH_LONG).show();
                    FirebaseAuth.getInstance().signOut();
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                }
            });

            final TextView nameTextView = (TextView) view.findViewById(R.id.fullName);
            final TextView emailTextView = (TextView) view.findViewById(R.id.emailAddress);
            final TextView genreTextView = (TextView) view.findViewById(R.id.genre);

            //Displays user information.
            reference.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    User userAccount = snapshot.getValue(User.class);

                    if (userAccount != null) {
                        String name = userAccount.fullName;
                        String email = userAccount.email;
                        String genre = userAccount.genre;

                        nameTextView.setText(name);
                        emailTextView.setText(email);
                        genreTextView.setText(genre);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_LONG).show();
                }
            });
        }
        // Inflate the layout for this fragment
        return view;

    }
}
