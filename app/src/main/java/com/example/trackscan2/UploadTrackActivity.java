package com.example.trackscan2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class UploadTrackActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView logo, uploadMessage;
    private Button uploadTrack, backToHome;
    private EditText editTexttrackTitle, editTextartistName, editTexttrackLength, editTextdate;
    private Spinner dropDownGenre, dropDownPlatform;

    private DatabaseReference database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_track);

        database = FirebaseDatabase.getInstance().getReference().child("Tracks");

        ////Finding the views by ID and setting onclick listeners.
        logo = (TextView) findViewById(R.id.uploadText);
        uploadMessage = (TextView) findViewById(R.id.messageText);

        uploadTrack = (Button) findViewById(R.id.uploadButton);
        uploadTrack.setOnClickListener(this);

        backToHome = (Button) findViewById(R.id.backButton);
        backToHome.setOnClickListener(this);

        editTexttrackTitle = (EditText) findViewById(R.id.title);
        editTextartistName = (EditText) findViewById(R.id.artist);
        editTexttrackLength = (EditText) findViewById(R.id.length);
        editTextdate = (EditText) findViewById(R.id.date);
        dropDownGenre = (Spinner) findViewById(R.id.trackGenre);
        dropDownPlatform = (Spinner) findViewById(R.id.trackPlatform);
    }

    //On Click Listeners for Activity
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backButton:
                startActivity(new Intent(this, MainActivity.class));
                break;
            case R.id.uploadButton:
                uploadTrack();
        }

    }

    private void uploadTrack() {

        //declaring strings for Activity
        String trackTitle = editTexttrackTitle.getText().toString();
        String trackArtistName = editTextartistName.getText().toString();
        String trackLength = editTexttrackLength.getText().toString();
        String trackDate = editTextdate.getText().toString();
        String trackGenre = dropDownGenre.getSelectedItem().toString();
        String trackPlatform = dropDownPlatform.getSelectedItem().toString();

        //if statements to make sure there are no blank entries
        if (trackTitle.isEmpty()){
            editTexttrackTitle.setError("Track title is required!");
            editTexttrackTitle.requestFocus();
            return;
        }

        if (trackArtistName.isEmpty()) {
            editTextartistName.setError("Track Artist is required");
            editTextartistName.requestFocus();
            return;
        }

        if (trackLength.isEmpty()){
            editTexttrackLength.setError("Track length is required!");
            editTexttrackLength.requestFocus();
            return;
        }

        if (trackDate.isEmpty()) {
            editTextdate.setError("Todays date is required");
            editTextdate.requestFocus();
            return;
        }

        Track track = new Track(trackTitle, trackArtistName, trackLength, trackDate, trackGenre, trackPlatform);

        //pushing data to database
        database.push().setValue(track);
        startActivity(new Intent(UploadTrackActivity.this, MainActivity.class));
        Toast.makeText(UploadTrackActivity.this,"Track has been uploaded!", Toast.LENGTH_LONG).show();
    }
}