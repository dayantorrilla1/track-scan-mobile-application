package com.example.trackscan2;

public class User {

    public String fullName, phoneNumber, email, genre;

    public User(){

    }

    public User(String fullName, String phoneNumber, String email, String genre){
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.genre = genre;
    }
}
