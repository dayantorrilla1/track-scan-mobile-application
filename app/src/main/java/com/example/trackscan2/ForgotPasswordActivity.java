package com.example.trackscan2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    //Declaring variables
    private ImageView logo;
    private TextView loginText, registerText;
    private EditText editTextEmail;
    private Button resetPasswordButton;

    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        //Finding the views by ID and setting onclick listeners.
        logo = findViewById(R.id.logo);
        editTextEmail = findViewById(R.id.email_input);

        resetPasswordButton = findViewById(R.id.sendButton);
        resetPasswordButton.setOnClickListener(this);
        loginText = findViewById(R.id.login_text);
        loginText.setOnClickListener(this);
        registerText = findViewById(R.id.register_text);
        registerText.setOnClickListener(this);

        //FirebaseAuth object.
        mAuth = FirebaseAuth.getInstance();
        
        
    }

    @Override
    //On click listeners for this page.
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.logo:
                startActivity(new Intent(this, MainActivity.class));
                break;
            case R.id.login_text:
                loginText.setTextColor(Color.BLUE);
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.register_text:
                registerText.setTextColor(Color.BLUE);
                startActivity(new Intent(this, RegisterUser.class));
                break;
            case R.id.sendButton:
                resetPassword();
                break;
        }
    }

    private void resetPassword() {
        String email = editTextEmail.getText().toString().trim();

        //Parameters to make sure no blanks are inputted
        if (email.isEmpty()) {
            editTextEmail.setError("Email is required!");
            editTextEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextEmail.setError("Please provide a valid email!");
            editTextEmail.requestFocus();
            return;
        }

        //Sends an email to reset the password of a particular account.
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {
                    Toast.makeText(ForgotPasswordActivity.this, "Check your email to reset your password!", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(ForgotPasswordActivity.this, LoginActivity.class));
                }else{
                    Toast.makeText(ForgotPasswordActivity.this, "Something went wrong! Please try again!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
