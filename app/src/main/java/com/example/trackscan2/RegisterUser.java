package com.example.trackscan2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

public class RegisterUser extends AppCompatActivity implements View.OnClickListener {

    private TextView logo, registerMessage, alreadyHaveAccount, registerUser;
    private EditText editTextfullname,editTextemail, editTextpassword, editTextphoneNumber;
    private Spinner spinnerGenre;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        mAuth = FirebaseAuth.getInstance();

        logo = (TextView) findViewById(R.id.logo);
        registerMessage = (TextView) findViewById(R.id.registerMessage);

        registerUser = (Button) findViewById(R.id.registerUser);
        registerUser.setOnClickListener(this);

        alreadyHaveAccount = (TextView) findViewById(R.id.alreadyHaveAccount);
        alreadyHaveAccount.setOnClickListener(this);

        editTextemail = (EditText) findViewById(R.id.email);
        editTextfullname = (EditText) findViewById(R.id.fullname);
        editTextpassword = (EditText) findViewById(R.id.password);
        editTextphoneNumber = (EditText) findViewById(R.id.phoneNumber);
        spinnerGenre = (Spinner) findViewById(R.id.genre);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.alreadyHaveAccount:
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.registerUser:
                registerUser();
                break;
        }

    }

    private void registerUser() {
        String fullName = editTextfullname.getText().toString().trim();
        String email = editTextemail.getText().toString().trim();
        String password = editTextpassword.getText().toString().trim();
        String phoneNumber = editTextphoneNumber.getText().toString().trim();
        String genre = spinnerGenre.getSelectedItem().toString().trim();

        if (fullName.isEmpty()){
            editTextfullname.setError("Full name is required!");
            editTextfullname.requestFocus();
            return;
        }

        if (email.isEmpty()) {
            editTextemail.setError("Email is required");
            editTextemail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            editTextemail.setError("Please provide valid email!");
            editTextemail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            editTextpassword.setError("Password is required");
            editTextpassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            editTextpassword.setError("Password length should be more than 6 characters");
            editTextpassword.requestFocus();
            return;
        }

        if (password.length() > 12) {
            editTextpassword.setError("Password length should not be more than 12 characters");
            editTextpassword.requestFocus();
            return;
        }

        if (phoneNumber.isEmpty()) {
            editTextphoneNumber.setError("Phone number is required");
            editTextphoneNumber.requestFocus();
            return;
        }

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()){
                            User user = new User(fullName, email, phoneNumber, genre);

                            FirebaseDatabase.getInstance().getReference("Users")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                    .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if (task.isSuccessful()){
                                                startActivity(new Intent(RegisterUser.this, LoginActivity.class));
                                                Toast.makeText(RegisterUser.this, "User has been registered successfully", Toast.LENGTH_LONG).show();

                                            }else{
                                                Toast.makeText(RegisterUser.this, "Failed to register, try again!", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });

                        }else{
                            Toast.makeText(RegisterUser.this, "Failed to register", Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }
}