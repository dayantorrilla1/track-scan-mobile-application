package com.example.trackscan2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

//Adapter class to display tracks from database
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    Context context;

    ArrayList<Track> list;

    public MyAdapter(Context context, ArrayList<Track> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Track track = list.get(position);
        holder.trackTitle.setText(track.getTrackTitle());
        holder.trackArtist.setText(track.getTrackArtist());
        holder.trackGenre.setText(track.getTrackGenre());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView trackTitle, trackArtist, trackGenre;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            trackTitle = itemView.findViewById(R.id.tvTrackTitle);
            trackArtist = itemView.findViewById(R.id.tvTrackArtist);
            trackGenre = itemView.findViewById(R.id.tvTrackGenre);
        }
    }
}
