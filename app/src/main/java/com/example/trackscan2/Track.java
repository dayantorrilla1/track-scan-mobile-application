package com.example.trackscan2;

public class Track {

    String trackTitle, trackArtist, trackLength, uploadDate, trackGenre, streamingPlatform;

    public Track(String trackTitle, String trackArtist, String trackLength, String uploadDate, String trackGenre, String streamingPlatform) {
        this.trackTitle = trackTitle;
        this.trackArtist = trackArtist;
        this.trackLength = trackLength;
        this.uploadDate = uploadDate;
        this.trackGenre = trackGenre;
        this.streamingPlatform = streamingPlatform;
    }

    public Track(){}

    public String getTrackTitle() {
        return trackTitle;
    }

    public String getTrackArtist() {
        return trackArtist;
    }

    public String getTrackLength() {
        return trackLength;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public String getTrackGenre() {
        return trackGenre;
    }

    public String getStreamingPlatform() {
        return streamingPlatform;
    }
}